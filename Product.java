public class Product {
    private String name;
    private double price;
    private double rating;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
        this.rating = 0;
    }
    public final String getName() {
        return this.name;
    }
    public final double getPrice() {
        return this.price;
    }
    public final double getRating() {
        return this.rating;
    }
    public final void setName(String name) {
        this.name = name;
    }
    public final void setPrice(double price) {
        this.price = price;
    }
    public final void setRating(double rating) {
        this.rating = rating;
    }
}
