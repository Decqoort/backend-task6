public class User {
    private String login;
    private String password;
    private Basket basket = new Basket();

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public void putProductInThe (Product product) {
        this.basket.addProduct(product);
    }
    public void showBasket(){
        this.basket.showBasket();
        this.showTotalPrice();
    }
    public void showTotalPrice(){
        System.out.println("Total price: " + this.basket.totalPrice());
    }
}
