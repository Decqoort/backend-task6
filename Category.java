import java.util.ArrayList;

public class Category {
    private String name;
    private ArrayList<Product> products;

    public Category(String name) {
        this.name = name;
        this.products = new ArrayList<Product>();
    }
    public void addProduct (Product product) {
        this.products.add(product);
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
    public ArrayList<Product> getProducts(){
        return this.products;
    }
}
