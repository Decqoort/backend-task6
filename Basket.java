import java.util.ArrayList;

public class Basket {
    private ArrayList<Product> basket;
    public Basket(){
        this.basket = new ArrayList<Product>();
    }
    public void addProduct(Product product) {
        this.basket.add(product);
    }
    public void removeProduct(String name) {
        int i = 0;
        while (i < this.basket.size()){
            if (this.basket.get(i).getName().equals(name)) {
                this.basket.remove(i);
            }
            i++;
        }
    }
    public void removeAllProducts() {
        this.basket = new ArrayList<Product>();
    }
    public void showBasket() {
        for(Product elem: this.basket){
            System.out.println(elem.getName() + " - " + elem.getPrice());
        }
    }
    public double totalPrice(){
        double price = 0;
        for(Product elem: this.basket){
            price += elem.getPrice();
        }
        return price;
    }
}
