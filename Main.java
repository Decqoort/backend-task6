public class Main {
    public static void main(String[] args) {
        Product bread = new Product("Bread", 34);
        Product apple = new Product("Apple", 25);
        Product orange = new Product("Orange", 42);
        Product potato = new Product("Potato", 36);
        Product tomato = new Product("Tomato", 28);
        Product cucumber = new Product("Cucumber", 26);

        Category bakery = new Category("Bakery");
        Category fruits = new Category("Fruits");
        Category vegetables = new Category("Vegetables");

        bakery.addProduct(bread);
        fruits.addProduct(apple);
        fruits.addProduct(orange);
        vegetables.addProduct(potato);
        vegetables.addProduct(tomato);
        vegetables.addProduct(cucumber);

        User user1 = new User("user1", "123user1");
        user1.putProductInThe(bread);
        user1.putProductInThe(apple);
        user1.putProductInThe(cucumber);
        user1.showBasket();
    }
}
